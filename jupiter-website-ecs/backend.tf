# store the terraform state file in s3
terraform {
  backend "s3" {
    bucket    = "franka-terraform-remote-statefile"
    key       = "jupiter-website-ecs.tfstatefile"
    region    = "us-east-1"
    profile   = "Katim"
  }
}